<?php
	get_header();
	the_post();
?>

	<main id="contacts" class="main">
		<div class="container">
			<header class="page-title">
				<h2><?php the_title();?></h2>
			</header>
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<address class="contacts-address">
						<?=get_option('endereco')?><br/>
						CEP:<?=get_option('cep')?><br/>
						CNPJ:<?=get_option('cnpj')?><br/>
						<?=get_option('contato')?>
            		</address>

					<header class="contacts-map-title">
						<h3><i>Localização</i></h3>
					</header>
					<div id="map"></div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<!-- <div class="contacts-form">
						<header class="contacts-form-title">
							<h3>Envie um email</h3>
						</header>
						<form action="#" method="post" class="contacts-form-fields">
							<label for="nome"><strong>Nome</strong></label>
							<input type="text" id="nome" name="nome">

							<label for="email"><strong>Email</strong></label>
							<input type="text" id="email" name="email" >
							<small>(email válido)</small>

							<label for="assunto"><strong>Assunto</strong></label>
							<input type="text" id="assunto" name="assunto">
							<small>(obrigatório)</small>

							<label for="mensagem"><strong>Mensagem</strong></label>
							<textarea id="mensagem" name="mensagem"></textarea>
							<small>(obrigatório)</small>
							<button type="submit" class="contacts-form-fields-button">Enviar</button>
						</form>
					</div> -->
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</main>
	<script>
		var mapConfig = {
			"address": "<?php echo get_option('endereco'); ?>"
		}
	</script>
<?php get_footer(); ?>