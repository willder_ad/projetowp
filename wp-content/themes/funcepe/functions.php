<?php

define("THEME_PATH", get_bloginfo("template_directory") . "/");
define("CSS_PATH", THEME_PATH . "css/");
define("JS_PATH", THEME_PATH . "js/");
define("IMG_PATH", THEME_PATH . "img/");

add_theme_support('menus');
add_theme_support( 'post-thumbnails');
//Metaboxes mais simples de concursos
add_action( 'init', 'create_post_type' );
add_action( 'add_meta_boxes', 'meta_boxes');
add_action( 'save_post', 'concurso_save_meta_boxes_data' );
add_action('admin_menu', 'add_site_custom_options');

//Galeria/Attachs
add_action( 'admin_head-post.php', 'print_scripts' );
add_action( 'admin_head-post-new.php', 'print_scripts' );
add_action( 'save_post', 'save_gallery', 10, 2 );
add_action( 'save_post', 'save_attach', 10, 2 );

function create_post_type() {
  register_post_type("concursos", [
  		"labels" => ["name" => __("Concursos"), "singular_name" => __("Concurso")],
  		"public" => true,
  		"has_archive" => true,
  		"rewrite" => ["slug" => "concursos"],
  		"supports" => ["title", "editor","thumbnail","excerpt"],
  		"taxonomies" => ["post_categoria"]
  ]);

  register_post_type("selecoes", [
  		"labels" => ["name" => __("Seleções"), "singular_name" => __("Seleção")],
  		"public" => true,
  		"has_archive" => true,
  		"rewrite" => ["slug" => "selecoes"],
  		"supports" => ["title", "editor","excerpt"],
  		"taxonomies" => ["post_categoria"]
  ]);

  register_post_type("galeria", [
  		"labels" => ["name" => __("Galeria"), "singular_name" => __("Galeria")],
  		"public" => true,
  		"has_archive" => true,
  		"rewrite" => ["slug" => "galeria"],
  		"supports" => ["thumbnail"]
  ]);

  register_taxonomy(
	'gallery_categories',
	["galeria"],
		[
			'hierarchical' => true,
			'label' => 'Categoria',
			'show_ui' => true,
			'show_in_tag_cloud' => true,
			'query_var' => true,
			'rewrite' => array('slug' => 'categoria'),
		]
	);

  register_taxonomy(
	'categories',
	["concursos","selecoes"],
		[
			'hierarchical' => true,
			'label' => 'Categoria',
			'show_ui' => true,
			'show_in_tag_cloud' => true,
			'query_var' => true,
			'rewrite' => array('slug' => 'categoria'),
		]
	);
}

function add_site_custom_options(){
    add_options_page('Configurações do site',
			    	 'Configurações do site',
			    	 'manage_options',
			    	 'opcoes',
			    	 'site_custom_options');
}

function meta_boxes() {
	add_meta_box(
		'inscricoes_meta_box',
		'Detalhes do Concurso',
		'inscricoes_callback',
		'concursos',
		'normal',
		'high'
	);
	add_meta_box(
		'endereco_meta_box',
		'Endereço do concurso',
		'endereco_callback',
		'concursos',
		'normal',
		'high'
	);
	add_meta_box(
		'resultado_meta_box',
		'Link do Resultado',
		'resultado_callback',
		'concursos',
		'side',
		'low'
	);
	add_meta_box(
		'attach_meta_box',
		'Anexos',
		'attach_callback',
		'concursos',
		'normal',
		'low'
	);
	add_meta_box(
		'galeria_meta_box',
		'Galeria',
		'gallery_callback',
		'galeria',
		'normal',
		'high'
	);
}

function inscricoes_callback( $post ) {

	wp_nonce_field( 'inscricoes_meta_box', 'inscricoes_meta_box_nonce' );
	$html = "";
	$data_inicial = get_post_meta($post->ID,"data_inicial",true);
	$data_inicial !== "" ? $data_inicial : $data_inicial = date("Y-m-d");
	$data_final = get_post_meta($post->ID,"data_final",true);
	$data_final !== "" ? $data_final : $data_final = date("Y-m-d");

	$html .= "<label for='data_inicial'>Inscrições:</label><br/>";
	$html .= "<input type='date' style='width:48%;' name='data_inicial' value='".date("Y-m-d",strtotime($data_inicial))."'/> à ";
	$html .= "<input type='date' style='width:48%;' name='data_final' value='".date("Y-m-d",strtotime($data_final))."'/>";

	echo $html;
}


function endereco_callback( $post ) {

	wp_nonce_field( 'endereco_meta_box', 'endereco_meta_box_nonce' );
	$html = "";
	$endereco = get_post_meta($post->ID,"endereco_concurso",true);

	$html .= "<label for='endereco'>Endereço:</label><br/>";
	$html .= "<input type='text' name='endereco_concurso' style='width:100%;' value='".$endereco."' />";

	echo $html;
}

function resultado_callback( $post ) {

	wp_nonce_field( 'resultado_meta_box', 'resultado_meta_box_nonce' );
	$html = "";
	$resultado = get_post_meta($post->ID,"resultado_concurso",true);

	$html .= "<label for='resultado'>Link do resultado:</label><br/>";
	$html .= "<input type='text' name='resultado_concurso' style='width:100%;' value='".$resultado."' />";

	echo $html;
}

function gallery_callback( $post ) {

	$gallery_data = get_post_meta($post->ID,"gallery_data",true);
	wp_nonce_field("galeria_meta_box","galeria_meta_box_nonce");
?>
    <input type="button" class="button button-primary" value="Adicionar Imagem" onclick="add_image();" />
    <hr>
    <div id="box-gallery">
<?php

	if(isset($gallery_data["image_url"])) :
		for($i = 0;$i < count($gallery_data["image_url"]);$i++) :
?>
			<div id="gallery-item" class="text-center">
				<input type="hidden" name="gallery[image_url][]" value="<?php esc_html_e($gallery_data['image_url'][$i]) ?>">
                <input type="hidden" name="gallery[image_id][]" value="<?php esc_html_e($gallery_data['image_id'][$i]) ?>">
				<figure class="images">
					<img src="<?php esc_html_e($gallery_data['image_url'][$i]); ?>">
				</figure>
				<label for="gallery[image_link][]">Link (opcional):</label>
				<input type="text" name="gallery[image_link][]" value="<?php esc_html_e($gallery_data['image_link'][$i]) ?>">
				<button class='button' onclick="remove_field(this)">Remover</button>
			</div>
<?php
		endfor;
	endif;
?>
	</div>
	<div id="box-gallery"></div>
	<div class="clear"></div>
<?php
}

function attach_callback( $post ){
	$attach = get_post_meta($post->ID,"attach",true);
	wp_nonce_field("attach_meta_box","attach_meta_box_nonce");
?>
	<input type="button" value="Adicionar Anexo" class="button button-primary" onclick="add_attach();" />
	<hr>
	<div id="box-attach">
<?php
	if(isset($attach["attach_name"])) :
		for($i = 0;$i < count($attach["attach_name"]);$i++) :
?>
			<div id="attach-item" class="text-center">
                <figure>
	                <img src='<?=IMG_PATH?>/icones/text.png' >
	            </figure>
                <input type="hidden" name="attach[attach_id][]" value="<?php esc_html_e($attach['attach_id'][$i]) ?>">
                <input type="hidden" name="attach[attach_title][]" value="<?php esc_html_e($attach['attach_title'][$i]) ?>">
                <input type="hidden" name="attach[attach_name][]" value="<?php esc_html_e($attach['attach_name'][$i]) ?>">
				<p><?php esc_html_e($attach['attach_title'][$i]) ?></p>
				<button class='button' onclick="remove_field(this)">Remover</button>
			</div>
<?php
		endfor;
	endif;
?>
	</div>
	<div id="box-attach"></div>
	<div class="clear"></div>
<?php
}

function print_scripts($post){
?>
	<style type="text/css">
		#gallery-item,
		#attach-item{
			width:180px;
			float: left;
			border:1px solid #ddd;
			background-color: #f7f7f7;
			margin: 5px;
			border-radius: 3px;
			padding: 5px;
		}
		.images img{
			width: 100px;
			height: 100px;
		}
		.text-center{
			text-align: center;
		}
    </style>

    <script type="text/javascript">
        function add_image() {
            var fileFrame = wp.media.frames.file_frame = wp.media({
                multiple: true,
                library: {
                	type: "image"
                }
            });
            fileFrame.on('select', function() {
                var selection = fileFrame.state().get('selection').toJSON();
                selection.map(function(attachment){
                	console.log(attachment);
                	jQuery(document.getElementById("box-gallery")).append(
                		"<div id='gallery-item' class='text-center'>" +
                			"<input type='hidden' name='gallery[image_url][]' value='"+attachment.url+"'>" +
                			"<input type='hidden' name='gallery[image_id][]' value='"+attachment.id+"'>" +
	                		"<figure class='images'>" +
	                			"<img src='"+attachment.url+"' >" +
	                		"</figure>" +
	                		"<label for='gallery[image_link][]'>Link (opcional): "+
	                		"<input type='text' name='gallery[image_link][]' style='width:95%'>" +
	                		"<button class='button' onclick='remove_field(this)'>Remover</button>" +
	                	"</div>"
                	);
                });
            });
            fileFrame.open();
        };

        function add_attach() {
            var fileFrame = wp.media.frames.file_frame = wp.media({
                multiple: true,
                library: {
                	type: "file"
                }
            });
            fileFrame.on('select', function() {
                var selection = fileFrame.state().get('selection').toJSON();
                selection.map(function(attachment){
                	console.log(attachment);
                	jQuery(document.getElementById("box-attach")).append(
                		"<div id='attach-item' class='text-center'>" +
                			"<figure>" +
	                			"<img src='<?=IMG_PATH?>/icones/text.png' >" +
	                		"</figure>" +
                			"<input type='hidden' name='attach[attach_id][]' value='"+attachment.id+"'>" +
                			"<input type='hidden' name='attach[attach_title][]' value='"+attachment.title+"'>" +
                			"<input type='hidden' name='attach[attach_name][]' value='"+attachment.filename+"'>" +
                			"<p>"+attachment.title+"</p>" +
	                		"<button class='button' onclick='remove_field(this)'>Remover</button>" +
	                	"</div>"
                	);
                });
            });
            fileFrame.open();
        };

        function remove_field(obj) {
        	var opt = confirm("Deseja mesmo remover este item?");

        	if(opt){
	            var parent=jQuery(obj).parent().parent();
	            parent.remove();
        	}else{
        		return;
        	}
        }
    </script>
<?php
}

function save_gallery($post_id){
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !wp_verify_nonce( $_POST['galeria_meta_box_nonce'], "galeria_meta_box"))
        return;
    if (get_post_type($post_id) !== "galeria")
        return;
    if ( $_POST['gallery'] )
    {
        $gallery_data = array();
        for ($i = 0; $i < count( $_POST['gallery']['image_url'] ); $i++ )
        {
            if ( '' != $_POST['gallery']['image_url'][ $i ] )
            {
                $gallery_data['image_url'][]  = $_POST['gallery']['image_url'][ $i ];
                $gallery_data['image_id'][]  = $_POST['gallery']['image_id'][ $i ];
                $gallery_data['image_link'][]  = $_POST['gallery']['image_link'][ $i ];
            }
        }

        if ( $gallery_data )
            update_post_meta( $post_id, 'gallery_data', $gallery_data );
        else
            delete_post_meta( $post_id, 'gallery_data' );
    }
    else
    {
        delete_post_meta( $post_id, 'gallery_data' );
    }
}

function save_attach($post_id){
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !wp_verify_nonce( $_POST['attach_meta_box_nonce'], "attach_meta_box"))
        return;
    if (get_post_type($post_id) !== "concursos")
        return;
    if ( $_POST['attach'] )
    {
        $gallery_data = array();
        for ($i = 0; $i < count( $_POST['attach']['attach_name'] ); $i++ )
        {
            if ( '' != $_POST['attach']['attach_name'][ $i ] )
            {
                $gallery_data['attach_id'][]  = $_POST['attach']['attach_id'][ $i ];
                $gallery_data['attach_title'][]  = $_POST['attach']['attach_title'][ $i ];
                $gallery_data['attach_name'][]  = $_POST['attach']['attach_name'][ $i ];
            }
        }

        if ( $gallery_data )
            update_post_meta( $post_id, 'attach', $gallery_data );
        else
            delete_post_meta( $post_id, 'attach' );
    }
    else
    {
        delete_post_meta( $post_id, 'attach' );
    }
}

function concurso_save_meta_boxes_data( $post_id ) {

	if ( ! isset( $_POST['inscricoes_meta_box_nonce'] ) ||
		! isset( $_POST['endereco_meta_box_nonce'] ) ||
		! isset($_POST['resultado_meta_box_nonce'])) {
		return;
	}
	if ( ! wp_verify_nonce( $_POST['inscricoes_meta_box_nonce'], 'inscricoes_meta_box' ) ||
		 ! wp_verify_nonce( $_POST['endereco_meta_box_nonce'], 'endereco_meta_box' ) ||
		 ! wp_verify_nonce($_POST['resultado_meta_box_nonce'], 'resultado_meta_box')) {
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	if ( isset( $_POST['post_type'] ) && 'contato' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	$data_inicial = isset($_POST['data_inicial']) ? $_POST['data_inicial'] : null;
	$data_final = isset($_POST['data_final']) ? $_POST['data_final'] : null;
	$endereco = isset($_POST['endereco_concurso']) ? $_POST['endereco_concurso'] : null;
	$resultado = isset($_POST['resultado_concurso']) ? $_POST['resultado_concurso'] : null;

	update_post_meta( $post_id, 'data_inicial', $data_inicial);
	update_post_meta( $post_id, 'data_final', $data_final);
	update_post_meta( $post_id,'endereco_concurso',$endereco);
	update_post_meta( $post_id, 'resultado_concurso', $resultado);
}

function site_custom_options(){
	$html = '';

    $html .= '<div class="wrap">';
    $html .=     '<h2>Global Custom Options</h2>';
    $html .=     '<form method="post" action="options.php">';
    $html .=         ''. wp_nonce_field('update-options') .'';
    $html .=         '<p><strong>Endereço:</strong><br />';
    $html .=             '<input type="text" name="endereco" size="45" value="'. get_option('endereco') .'" />';
    $html .=         '</p>';
    $html .=         '<p><strong>CEP:</strong><br />';
    $html .=             '<input type="text" name="cep" size="45" value="'. get_option('cep') .'" />';
    $html .=         '</p>';
    $html .=         '<p><strong>CNPJ:</strong><br />';
    $html .=             '<input type="text" name="cnpj" size="45" value="'. get_option('cnpj') .'" />';
    $html .=         '</p>';
    $html .=         '<p><strong>Contato:</strong><br />';
    $html .=             '<input type="text" name="contato" size="45" value="'. get_option('contato') .'" />';
    $html .=         '</p>';
    $html .=         '<p><input type="submit" name="Submit" value="Salvar Opções" class="button button-primary"/></p>';
    $html .=         '<input type="hidden" name="action" value="update" />';
    $html .=         '<input type="hidden" name="page_options" value="endereco,cep,cnpj,contato" />';
    $html .=     '</form>';
    $html .= '</div>';

    echo $html;
}

function wp_get_attachment( $attachment_id ) {

    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}