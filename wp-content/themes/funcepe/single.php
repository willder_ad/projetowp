<?php get_header(); ?>
	<main id="news-details" class="main">
		<div class="container">
			<header class="page-title">
				<div class="row">
					<div class="col-xs-5 col-sm-8 col-md-8">
						<h2>Notícias</h2>
					</div>
					<div class="col-xs-7 col-sm-4 col-md-4 margin-top-more text-right">
						<a href="<?=home_url();?>/noticias/" class="page-title-button">Voltar para Listagem</a>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-xs-12 col-sm-7 col-md-8">
					<header class="news-details-title margin-bottom">
						<h3><?php the_title(); ?></h3>
						<time class="news-details-title-date"><?=get_the_date();?></time>
					</header>
					<?php the_post(); ?>
					<article class="news-details-text">
						<p><?php the_content(); ?></p>
						<?php if(has_post_thumbnail()) : ?>
							<div class="row">
								<div class="col-xs-12 col-sm-7 col-md-5">
									<figure class="news-details-text-img box-bordered">
										<?php the_post_thumbnail()?>
									</figure>
								</div>
							</div>
						<?php endif; ?>
					</article>
				</div>
				<div class="col-xs-12 col-sm-5 col-md-4">
					<div id="news" class="box">
                        <header class="box-title">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <h2>Noticias</h2>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 margin-top-20">
                                    <a href="<?=home_url();?>/noticias/" title="Todos as noticias">ver todos</a>
                                </div>
                            </div>
                        </header>
                        <div class="box-item">
                            <?php
                            	query_posts(["post_type" => "post","posts_per_page" => 10,"post__not_in" =>[get_the_ID()]]);
                            	if(have_posts()):
                            		while(have_posts()) :
                            			the_post();
                            ?>
		                                <div class='row margin-top-20 item-border-bottom'>
		                                <?php if(has_post_thumbnail(get_the_ID())) : ?>
		                                	<div class='col-xs-4 col-sm-4 col-md-4'>
		                                        <figure class='box-item-img box-bordered'>
		                                            <?php the_post_thumbnail(); ?>
		                                        </figure>
		                                    </div>
		                                <?php else : ?>
		                                    <div class='col-xs-4 col-sm-4 col-md-4'>
		                                        <figure class='box-item-img box-bordered'>
		                                            <img src='<?=IMG_PATH?>logo_funcepe/funcepe_default.jpg' alt='FUNCEPE'>
		                                        </figure>
		                                    </div>
		                                <?php endif; ?>
		                                    <div class='col-xs-8 col-sm-8 col-md-8'>
		                                        <div class='box-item-text'>
		                                            <a href='<?php the_permalink(); ?>' class='box-item-link' title='Sessão solene em homenagem ao IEP...'><?php the_title(); ?></a>
		                                            <time class='box-item-date'><?=get_the_date();?></time>
		                                        </div>
		                                    </div>
		                                </div>
                            <?php
                            		endwhile;
                            	endif;
                            	wp_reset_query();
                            ?>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>