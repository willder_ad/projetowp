<html>
<head>
    <title><?php wp_title(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?=CSS_PATH?>primary-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-index-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-about-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-news-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-news-details-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-contests-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-contests-details-style.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>main-contacts-style.css" type="text/css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://developers.google.com/maps/documentation/javascript/demos/demos.css" type="text/css">
    <link rel="stylesheet" href="<?=CSS_PATH?>extern/bootstrap.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=CSS_PATH?>extern/jquery.bxslider.css">
    <?php wp_head(); ?>
</head>
<body>
    <header id="header">
        <section class="main-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 xs-text-center">
                        <h1>
                            <figure class="header-logo">
                                <a href="<?=home_url();?>" title="FUNCEPE"><img src="<?=IMG_PATH?>logo_funcepe/logo-funcep.png" alt="FUNCEPE"></a>
                            </figure>
                        </h1>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-5 col-md-offset-4 margin-top-20">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="secondary-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12  text-center">
                        <nav class="navbar header-menu" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle">
                                    <span><i class="fa fa-bars margin-less-right"></i>MENU</span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="menu">
                                <?php wp_nav_menu(["menu" => "Menu", "menu_class" => "nav navbar-nav"]); ?>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    </header>