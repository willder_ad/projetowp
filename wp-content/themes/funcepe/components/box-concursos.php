<div id="contests" class="box">
	<header class="box-title">
		<div class="row">
			<div class="col-xs-8 col-sm-7 col-md-8">
				<h2><i class="box-title-icon"></i>Concursos</h2>
			</div>
			<div class="col-xs-4 col-sm-5 col-md-4 margin-top-20">
				<a href="<?=home_url();?>/concursos/" title="Todos os concursos">ver todos</a>
			</div>
		</div>
	</header>
	<div class="box-item">
		<?php
			query_posts(["post_type" => "concursos", "posts_per_page" => 10]);
			if(have_posts()) :
				while(have_posts()) :
					the_post();
					$status = get_the_terms(get_the_ID(),"categories");
					$data_inicial = get_post_meta(get_the_ID(),"data_inicial",true);
					$data_final = get_post_meta(get_the_ID(),"data_final",true);
		?>
						<div class='row item-border-bottom margin-top-20'>
							<div class='col-xs-4 col-sm-5 col-md-4'>
								<?php if(has_post_thumbnail(get_the_ID())) : ?>
									<figure class="box-item-img box-bordered">
										<?php the_post_thumbnail(); ?>
									</figure>
								<?php else : ?>
									<figure class="box-item-img box-bordered">
										<img src='<?=IMG_PATH?>logo_funcepe/funcepe_default.jpg' alt='Itaitinga'>
									</figure>
								<?php endif; ?>
							</div>
							<div class='col-xs-8 col-sm-7 col-md-8'>
								<div class='box-item-text'>
									<a href='#' class='box-item-link' title='Concurso Público Prefeitura Municipal de Itaitinga – CE'><?php the_title(); ?></a>
									<time class='box-item-date'>Inscrições: <?=date("d/m/Y",strtotime($data_inicial))?>  à <?=date("d/m/Y",strtotime($data_final))?></time>
									<p class='box-item-status'><?=$status[0]->name?></p>
				    			</div>
							</div>
						</div>
		<?php
				endwhile;
			endif;
			wp_reset_query();
		?>
	</div>
</div>