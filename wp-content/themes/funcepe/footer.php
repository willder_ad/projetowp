      <footer id="footer">
            <div class="container">
                  <div class="row footer-info">
                        <div class="col-xs-12 col-sm-5 col-md-4">
                              <figure>
                                    <a href="<?=home_url()?>"><img src="<?=IMG_PATH?>logo_funcepe/logo-footer.png" alt="FUNCEPE"></a>
                              </figure>
                              <address>
                                    <h3><?=get_option('endereco')?></h3>
                                    <h3>CEP:<?=get_option('cep')?></h3>
                                    <h3>CNPJ:<?=get_option('cnpj')?></h3>
                                    <h4><a href="tel:+<?=get_option('contato')?>"><?=get_option('contato')?></h4>
                              </address>
                       </div>
                        <div class="footer-menus">
                              <div class="col-xs-12 col-sm-3 col-md-2 col-md-offset-3">
                                    <h3>Menu</h3>
                                    <?php wp_nav_menu(["menu" => "Menu"]); ?>
                              </div>
                              <div class="col-xs-12 col-sm-4 col-md-3">
                                    <h3>Institucional</h3>
                                    <?php wp_nav_menu(["menu" => "Institucional"]); ?>
                              </div>
                        </div>
                  </div>
            </div>
      </footer>

      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
      <script src="http://malsup.github.com/jquery.cycle2.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUQ-cxsaKEpKJSfmUQGMisXmqT2zOFDkE&callback=initMap" async defer></script>
      <script type="text/javascript" src="<?=JS_PATH?>responsive-menu.js"></script>
      <script type="text/javascript" src="<?=JS_PATH?>extern/jquery.bxslider.js"></script>
      <script type="text/javascript" src="<?=JS_PATH?>banner-slider.js"></script>
      <script type="text/javascript" src="<?=JS_PATH?>map.js"></script>
      <?php wp_footer(); ?>
</body>
</html>