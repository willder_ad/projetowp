<?php get_header(); ?>
    <main id="main" class="main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 pull-right">
                    <div class="main-slider">
                        <?php
                            query_posts([
                                "post_type" => "galeria",
                                "gallery_categories" => "banner"
                            ]);

                            if(have_posts()) :
                                the_post();
                                $gallery_data = get_post_meta(get_the_ID(),"gallery_data",true);
                                if(isset($gallery_data["image_url"])) :
                        ?>
                                    <ul class="banner-slider">
                                        <?php
                                            for($i = 0;$i < count($gallery_data["image_url"]);$i++) :
                                                $attach = wp_get_attachment($gallery_data["image_id"][$i]);
                                        ?>
                                                <li>
                                                  <a href="<?php esc_html_e($gallery_data['image_link'][$i]);?>" title="<?=$attach['title']?>">
                                                    <img src="<?php esc_html_e($gallery_data['image_url'][$i]);?>" title="<?=$attach['title']?>" alt="<?=$attach['alt']?>">
                                                  </a>
                                                </li>
                                        <?php endfor; ?>
                                    </ul>
                        <?php
                               endif;
                            endif;
                            wp_reset_query();
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <?php get_template_part("components/box", "concursos") ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div id="warnings" class="box">
                        <header class="box-title">
                            <h2>Avisos</h2>
                        </header>
                        <?php
                            query_posts([
                                "post_type" => "galeria",
                                "gallery_categories" => "avisos"
                            ]);

                            if(have_posts()) :
                                the_post();
                                $gallery_data = get_post_meta(get_the_ID(),"gallery_data",true);
                                if(isset($gallery_data["image_url"])) :
                                    //Padrão de um aviso na box
                                    $attach = wp_get_attachment($gallery_data["image_id"][0]);
                        ?>
                                    <figure class="box-warnings-img">
                                        <img src="<?php esc_html_e($gallery_data['image_url'][0]); ?>" alt="<?=$attach['alt']?>">
                                    </figure>
                        <?php
                                endif;
                            endif;
                            wp_reset_query();
                        ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div id="news" class="box">
                        <header class="box-title">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <h2>Noticias</h2>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 margin-top-20">
                                    <a href="noticias.php" title="Todos as noticias">ver todos</a>
                                </div>
                            </div>
                        </header>
                        <div class="box-item">
                            <?php
                                query_posts(["post_type" => "post", "posts_per_page" => 10]);
                                if (have_posts()) :
                                    while(have_posts()) :
                                        the_post();
                            ?>
                                        <div class='row margin-top-20 item-border-bottom'>
                                            <?php if(has_post_thumbnail(get_the_ID())) : ?>
                                                <div class='col-xs-4 col-sm-2 col-md-2'>
                                                    <figure class='box-item-img box-bordered'>
                                                        <?php the_post_thumbnail(); ?>
                                                    </figure>
                                                </div>
                                            <?php else : ?>
                                                <div class='col-xs-4 col-sm-2 col-md-2'>
                                                    <figure class='box-item-img box-bordered'>
                                                        <img src='<?=IMG_PATH?>logo_funcepe/funcepe_default.jpg' alt='FUNCEPE'>
                                                    </figure>
                                                </div>
                                            <?php endif; ?>
                                            <div class='col-xs-8 col-sm-10 col-md-10'>
                                                <div class='box-item-text'>
                                                    <a href='<?php the_permalink(); ?>' class='box-item-link' title='Sessão solene em homenagem ao IEP...'><?php the_title(); ?></a>
                                                    <p class='box-item-text'><?php the_excerpt(); ?></p>
                                                    <time class='box-item-date'><?=get_the_date();?></time>
                                                </div>
                                            </div>
                                        </div>
                            <?php
                                    endwhile;
                                endif;
                                wp_reset_query();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div id="selections" class="box">
                        <header class="box-title">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <h2>Seleções</h2>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 margin-top-20">
                                    <a href="#" title="Todos as Seleções">ver todos</a>
                                </div>
                            </div>
                        </header>
                        <div class="box-item">
                            <?php
                                query_posts(["post_type" => "selecoes", "posts_per_page" => 5]);
                                if(have_posts()) :
                                    while(have_posts()) :
                                        the_post();
                                        $status = get_the_terms(get_the_ID(),"categories");
                            ?>
                                        <div class='row margin-top-20 item-border-bottom padding-10'>
                                            <div class='col-xs-10 col-sm-10 col-md-10'>
                                                <p class='box-item-title'><?php the_title(); ?></p>
                                                <p class='box-item-status'><?=$status[0]->name?></p>
                                            </div>
                                            <div class='col-xs-2 col-sm-2 col-md-2'>
                                                <a href='#' class='box-item-link' title='Detalhes'>Detalhes</a>
                                            </div>
                                        </div>
                            <?php
                                    endwhile;
                                endif;
                                wp_reset_query();
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div id="agreements" class="box bg-gray-color padding-less-right padding-less-left">
                        <header class="box-title bg-gray-color item-border-bottom">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <h2>Convênios</h2>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 margin-top-20">
                                    <a href="#" title="Todos os Convênios">ver todos</a>
                                </div>
                            </div>
                        </header>
                        <div class="container-adjustment">
                            <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="2000"
                                 data-cycle-prev="#prev"
                                 data-cycle-next="#next">
                                <?php
                                    query_posts([
                                        "post_type" => "galeria",
                                        "gallery_categories" => "convenios"
                                    ]);

                                    if(have_posts()) :
                                        the_post();
                                        $gallery_data = get_post_meta(get_the_ID(),"gallery_data",true);
                                        if(isset($gallery_data["image_url"])) :
                                            for($i=0;$i < sizeof($gallery_data["image_url"]);$i++) :
                                                $attach = wp_get_attachment($gallery_data["image_id"][$i]);
                                ?>
                                                <img src="<?php esc_html_e($gallery_data["image_url"][$i]);?>" alt="<?=$attach['alt']?>">
                                <?php
                                            endfor;
                                        endif;
                                    endif;
                                    wp_reset_query();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div id="partners" class="box bg-gray-color padding-less-left padding-less-right">
                        <header class="box-title bg-gray-color item-border-bottom">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-8">
                                    <h2>Parceiros</h2>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 margin-top-20">
                                    <a href="#" title="Todos os Parceiros">ver todos</a>
                                </div>
                            </div>
                        </header>
                        <div class="container-adjustment">
                            <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="2000"
                                 data-cycle-prev="#prev"
                                 data-cycle-next="#next">
                                <?php
                                    query_posts([
                                        "post_type" => "galeria",
                                        "gallery_categories" => "parceiros"
                                    ]);

                                    if(have_posts()) :
                                        the_post();
                                        $gallery_data = get_post_meta(get_the_ID(),"gallery_data",true);
                                        if(isset($gallery_data["image_url"])) :
                                            for($i=0;$i < sizeof($gallery_data["image_url"]);$i++) :
                                                $attach = wp_get_attachment($gallery_data["image_id"][$i]);
                                ?>
                                                <img src="<?php esc_html_e($gallery_data['image_url'][$i]);?>" alt="<?=$attach['alt']?>">
                                <?php
                                            endfor;
                                        endif;
                                    endif;
                                    wp_reset_query();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php get_footer(); ?>