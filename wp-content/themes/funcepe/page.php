<?php
	get_header();
	the_post();

	$menu = wp_list_pages(["child_of" => get_the_ID(), "title_li" => null, "echo" => false, "include" => get_post_ancestors("")]);
	$col = $menu ? "col-xs-12 col-sm-8 col-md-9" : "col-xs-12 col-sm-12 col-md-12";
?>

	<main id="about" class="main">
		<div class="container">
			<header class="page-title">
				<h2><?php the_title(); ?></h2>
			</header>
			<div class="row">
			<?php if($menu) : ?>
				<aside class="col-xs-12 col-sm-4 col-md-3">
					<ul class="page-menu">
						<?=$menu?>
					</ul>
				</aside>
			<?php endif; ?>
				<div class="<?=$col?>">
					<article class="page-text">
						<?php the_content(); ?>
					</article>
				</div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>