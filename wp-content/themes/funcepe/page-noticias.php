<?php get_header(); ?>
	<main id="news" class="main">
		<div class="container">
			<header class="page-title">
				<h2><?php the_title(); ?></h2>
			</header>
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div id="news" class="news-box">
						<div class="news-box-item">
							<?php
								$paged = (get_query_var("paged")) ? get_query_var("paged") : 1;
								query_posts(["post_type" => "post",
											 "paged" => $paged,
											 "posts_per_page" => 5]);

								if(have_posts()) :
									while (have_posts()) :
										the_post();
							?>
										<div class="row item-border-bottom padding-bottom-10">
											<?php if(has_post_thumbnail(get_the_ID())) : ?>
			                                	<div class='col-xs-12 col-sm-4 col-md-3 margin-top-20'>
			                                        <figure class='news-box-item-img box-bordered'>
			                                            <?php the_post_thumbnail(); ?>
			                                        </figure>
			                                    </div>
			                                <?php else : ?>
			                                    <div class='col-xs-12 col-sm-4 col-md-3'>
			                                        <figure class='news-box-item-img box-bordered margin-top-20'>
			                                            <img src='<?=IMG_PATH?>logo_funcepe/funcepe_default.jpg' alt='FUNCEPE'>
			                                        </figure>
			                                    </div>
			                                <?php endif; ?>
											<div class="col-xs-12 col-sm-8 col-md-9">
												<a href="<?php the_permalink(); ?>" title="mais detalhes">
													<h3><?php the_title(); ?></h3>
												</a>
												<p class="news-box-item-text"><?php the_excerpt(); ?></p>
												<time class="news-box-item-date"><?=get_the_date()?></time><br>
												<a href="<?php the_permalink(); ?>" class="news-box-item-link margin-bottom" title="mais detalhes">mais detalhes</a>
											</div>
										</div>
							<?php
									endwhile;
								endif;
							?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-10 col-sm-offset-2 col-md-9 col-md-offset-3">
							<?php
								get_template_part("components/box","paginacao");
								wp_reset_query();
							?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
	            	<?php get_template_part("components/box","concursos"); ?>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>