<form role="search" method="get" class="search-form" action="<?=home_url();?>/concursos">
    <div class="search_group inline-block width-percent-90">
		<input type="text" name="pesquisa" placeholder="Pesquisar">
		<button type="submit">Buscar</button>
	</div>
    <a href="<?=home_url()?>/contatos/" class="about-button float-right" title="Envie-nos um email">
        <i class="fa fa-envelope"></i>
    </a>
</form>
