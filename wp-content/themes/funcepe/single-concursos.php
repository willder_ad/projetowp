<?php
	get_header();
	the_post();
?>
	<main id="contests-details" class="main">
		<div class="container">
			<header class="page-title">
				<div class="row">
					<div class="col-xs-5 col-sm-8 col-md-8">
						<h2>Concursos</h2>
					</div>
					<div class="col-xs-7 col-sm-4 col-md-4 margin-top-more text-right">
						<a href="<?=home_url();?>/concursos/" class="page-title-button">Voltar para Listagem</a>
					</div>
				</div>
			</header>
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<header class="contests-details-title">
						<h3><?php the_title(); ?></h3>
						<p>
							<time><?=get_the_date();?></time>
							/ 1052196 visitas até o momento<!-- Ver com o dorian -->
						</p>
					</header>
					<article class="contests-details-text" >

						<?php if(has_post_thumbnail()) : ?>
							<figure class="contests-details-text-img">
								<?php the_post_thumbnail(); ?>
							</figure>
						<?php else : ?>
							<figure class="contests-details-text-img">
								<img src='<?=IMG_PATH?>logo_funcepe/funcepe_default.jpg' alt='FUNCEPE'>
							</figure>
						<?php endif; ?>
						<?php
							the_content();
							$endereco_concurso = get_post_meta(get_the_ID(),'endereco_concurso',true);
							if ($endereco_concurso !== "") :
						?>
								<header class="contests-details-map-title">
									<h4><i>Localização</i></h4>
								</header>
								<div id="map"></div>
						<?php endif; ?>
					</article>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="contests-details-result-box">
						<header>
							<h2>
								<i>Acompanhe sua inscrição</i>
							</h2>
						</header>
						<ul>
							<li>
								<a href="<?=get_post_meta(get_the_ID(),"resultado_concurso",true)?>"><i class="fa fa-eye"></i> Resultado</a>
							</li>
						</ul>
					</div>
					<div class="contests-details-attachment-box">
						<header>
							<h2>
								<i>Edital, Anexos e Aditivos</i>
							</h2>
						</header>
						<ul>
							<?php
								$attach = get_post_meta(get_the_ID(), "attach",true);
								if(isset($attach["attach_name"])) :
									for($i = 0;$i < count($attach["attach_name"]);$i++) :
							?>
										<li>
											<a href="<?=wp_get_attachment_url($attach["attach_id"][$i]);?> " download="<?=$attach["attach_title"][$i]?>" target="_blank"><i class="fa fa-file-o"></i><i>&nbsp;<?php esc_html_e($attach["attach_title"][$i]); ?></i></a>
										</li>
							<?php
									endfor;
								endif;
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script>
		var mapConfig = {
			"address": "<?=$endereco_concurso?>"
		}
	</script>
<?php get_footer();?>
