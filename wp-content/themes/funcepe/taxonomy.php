<?php get_header();?>
	<main id="contests" class="main">
		<div class="container">
			<header class="page-title">
				<h2>Concursos</h2>
			</header>
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<?php
							$filter = isset($_GET["pesquisa"]) ? $_GET["pesquisa"] : "";
							$querried_object = get_queried_object();
							$paged = (get_query_var("paged")) ? get_query_var("paged") : 1;
							if($filter !== ""){
								query_posts([
										"post_type" => "concursos",
										"paged" => $paged,
										"posts_per_page" => 6,
										"categories" => $querried_object->slug,
										"s" => $filter
									]);
							}else{
								query_posts([
										"post_type" => "concursos",
										"paged" => $paged,
										"posts_per_page" => 6,
										"categories" => $querried_object->slug
									]);
							}
							if(have_posts()) :
								while(have_posts()) :
									the_post();
									$status = get_the_terms(get_the_ID(),"categories");
									$data_final = get_post_meta(get_the_ID(),"data_final",true);
						?>
									<div class="col-xs-12 col-sm-6 col-md-4">
										<div id="contests-box" class="box bg-gray-color text-center">
											<?php if(has_post_thumbnail(get_the_ID())) : ?>
			                                        <figure class='box-item-img'>
				                                        <a href="<?php the_permalink(); ?>" title="Detalhes">
				                                            <?php the_post_thumbnail(); ?>
				                                        </a>
			                                        </figure>
			                                <?php else : ?>
			                                        <figure class='box-item-img'>
			                                        	<a href="<?php the_permalink(); ?>" title="Detalhes">
			                                            	<img src='<?=IMG_PATH?>logo_funcepe/funcepe_default.jpg' alt='FUNCEPE'>
			                                            </a>
			                                        </figure>
			                                <?php endif; ?>
											<a href="<?php the_permalink(); ?>" title="Detalhes">
												<p class="box-item-desc"><?php the_title(); ?></p>
											</a>
											<p class="box-item-status"><?=$status[0]->name?><br><?=date("d/m/Y",strtotime($data_concurso));?></p>
											<a href="<?php the_permalink(); ?>" title="Detalhes" class="box-item-link">Ver detalhes</a>
										</div>
									</div>
						<?php
								endwhile;
							endif;
						?>
						<div class="col-xs-11 col-xs-offset-1 col-sm-10 col-sm-offset-2 col-md-9 col-md-offset-3">
							<?php
								get_template_part("components/box","paginacao");
								wp_reset_query();
							?>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4">
					<form role="search" method="get" class="contests-filter margin-top-30" action="<?=home_url();?>/concursos">
						<div class="search_group">
							<input type="text" name="pesquisa" placeholder="Filtrar">
							<button type="submit">Filtrar</button>
						</div>
					</form>
					<div class="contests-filter-box">
						<header class="contests-filter-box-title">
							<h3>Filtrar por Status</h3>
						</header>
						<ul class="contests-filter-box-options padding-less">
							<li>
								<i class="fa fa-circle status-four"></i>
								<a href="<?=home_url()?>/concursos/">Todos</a>
							</li>
							<?php
								$category = get_terms("categories");
								foreach ($category as $taxo) :
									$link = get_category_link($taxo->term_id);
							?>
									<li>
										<i class="fa fa-circle status-four"></i>
										<a href="<?=$link?>"><?=$taxo->name;?></a>
									</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>