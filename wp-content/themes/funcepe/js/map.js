function initMap() {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': mapConfig.address
    }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map = new google.maps.Map(document.getElementById("map"), {
            	zoom: 12,
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        }
    });
}