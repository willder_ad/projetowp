<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa user o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'funcepe');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!k4AjMMEJSU%eBcp4cS#m-Bn1)9ADkI rN#ri} )rr(#0q/g~6I/#J/T/1ye=0W]');
define('SECURE_AUTH_KEY',  'X>+jb%l[;HI c,2]ZSr<>_fn}S4hBQ?P9P 2h_X^mcjdRSM!9]Kqeu)vMmfly@[6');
define('LOGGED_IN_KEY',    '^$$]$&t^T[Y0E{KPP]NqNzW%fHZ0}^07hKT/Z}dzqPwnYUdO7qpZuPuY?{Zj7rhZ');
define('NONCE_KEY',        '*(hQwykOD&2Ej1d6#3visUHQuxm ;yJa$MEz8~Yvw*T*=9MxsM:opGOG%E@8]**a');
define('AUTH_SALT',        '}nd>h9%K`I.0C*gW*3]E,q>1Q(^Cq18gg+@O&;io)Bd:r* ;]v |`5mmvz@LcYkX');
define('SECURE_AUTH_SALT', 'LjNccy}TAA(kQp [h?`@6Q18y}b&ZqPf.+ 3LBoKsFvE8yB!KFk&dwK<W$F0%>nu');
define('LOGGED_IN_SALT',   'N8HY`!)Qvv>Pl0#uy5o+;nO/,OE_XWmS#}~Eb*ArU(nKkE0<Wn~BXXI}I0)_s%Q]');
define('NONCE_SALT',       'y.^fdw{T Q}aL*pQSisAgQP|^V.rIkm<`YkMx22]~e5~>;Fdvq:~VFHg9utbP8*:');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * para cada um um único prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
